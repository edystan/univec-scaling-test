#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <random>

#include <Eigen/Dense>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
// Univec includes
#include "qtydef/QtyDefinitions.hpp"
#include "univec/Matrix3.hpp"

#if defined(BLA_VENDOR_APPLE)
#include <Accelerate/Accelerate.h>
#elif defined(BLA_VENDOR_OPEN_BLAS)
#include <cblas.h>
#include <lapacke.h>
#endif

using namespace std;
using namespace dfpe;
using namespace boost::units;
using namespace boost::accumulators;
using namespace si;

using std::chrono::duration;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;
using std::chrono::nanoseconds;

#ifndef MATRIX_M
#error Please define MATRIX_M preprocessor variable
#endif
#ifndef MATRIX_N
#error Please define MATRIX_N preprocessor variable
#endif

constexpr int M = MATRIX_M;
constexpr int N = MATRIX_N;
constexpr int D = M * N;

using QtySiLengthPowD = boost::units::power_typeof_helper<QtySiLength, static_rational<N>>::type;

int main(int argc, char *argv[])
{
    random_device rd;
    mt19937 gen(rd());

    // Create a uniform distribution for generating random double values
    uniform_real_distribution<double> distribution(-1., 1);

    const int outer_cycles = 1000;  // Number of outer_cycles to run
    const int inner_cycles = 10000; // Number of inner_cycles to run

    // Define accumulators that will be used to calculate meanTime and stdDev for each implementation
    accumulator_set<double, features<tag::mean, tag::variance>> acc_raw;
    accumulator_set<double, features<tag::mean, tag::variance>> acc_sem;
    accumulator_set<double, features<tag::mean, tag::variance>> acc_eig;
    accumulator_set<double, features<tag::mean, tag::variance>> acc_bla;
    accumulator_set<double, features<tag::mean, tag::variance>> acc_uni;


    for (int i = 0; i < outer_cycles; ++i)
    {
        // -------------------------------- Generate working data for "Raw" -----------------------------------
        // Each element of the vector is an array of 9 elements corresponding to a 3x3 matrix values
        vector<array<double, D>> raw_input(inner_cycles);
        vector<double> raw_result(inner_cycles);

        for (int i = 0; i < inner_cycles; i++)
        {
            for (int d = 0; d < D; d++)
                raw_input[i][d] = distribution(gen);
        }

        // -------------------------------- Generate working data for "Semi" -----------------------------------
        vector<array<QtySiLength, D>> sem_input(inner_cycles);
        vector<QtySiLengthPowD> sem_result(inner_cycles);

        for (int i = 0; i < inner_cycles; i++)
        {
            for (int d = 0; d < D; d++)
                sem_input[i][d] = raw_input[i][d] * meter;
        }

        // -------------------------------- Generate working data for "Eigen" -----------------------------------
        vector<Eigen::Matrix<double, M, N, Eigen::RowMajor>> eig_input(inner_cycles);
        vector<double> eig_result(inner_cycles);

        for (int i = 0; i < inner_cycles; ++i)
        {
            for (int m = 0; m < M; m++)
                for (int n = 0; n < N; n++)
                    eig_input[i](m, n) = raw_input[i][m * N + n];
        }

        // -------------------------------- Generate working data for "Blas" ------------------------------------
        vector<array<double, D>> bla_input(inner_cycles);
        vector<double> bla_result(inner_cycles);

        for (int i = 0; i < inner_cycles; ++i)
        {
            for (int d = 0; d < D; d++)
                bla_input[i][d] = raw_input[i][d];
            bla_result[i] = 0.;
        }

        // -------------------------------- Generate working data for "Univec" ------------------------------------
        vector<RMatrix<M, N, QtySiLength>> uni_input(inner_cycles);
        vector<Real<QtySiLengthPowD>> uni_result(inner_cycles);

        for (int i = 0; i < inner_cycles; ++i)
        {
            for (int d = 0; d < D; d++)
                uni_input[i].rowMajor(d) = raw_input[i][d] * meter;
        }
        /*
        // -------------------------------- Start of "Raw" performance test -----------------------------------
        auto start_time_raw = high_resolution_clock::now();
        for (int j = 0; j < inner_cycles; ++j)
        {
            raw_result[j] = raw_input[j][0] * (raw_input[j][4] * raw_input[j][8] - raw_input[j][5] * raw_input[j][7]) -
                            raw_input[j][1] * (raw_input[j][3] * raw_input[j][8] - raw_input[j][5] * raw_input[j][6]) +
                            raw_input[j][2] * (raw_input[j][3] * raw_input[j][7] - raw_input[j][4] * raw_input[j][6]);
        }
        auto end_time_raw = high_resolution_clock::now();
        auto duration_raw = duration_cast<nanoseconds>(end_time_raw - start_time_raw).count();
        auto duration_raw_per_cycle = duration_raw / inner_cycles;
        acc_raw(duration_raw_per_cycle);
        // -------------------------------- End of "Raw" performance test -------------------------------------

        // -------------------------------- Start of "Semi" performance test -----------------------------------
        auto start_time_sem = high_resolution_clock::now();
        for (int j = 0; j < inner_cycles; ++j)
        {
            sem_result[j] = sem_input[j][0] * (sem_input[j][4] * sem_input[j][8] - sem_input[j][5] * sem_input[j][7]) -
                            sem_input[j][1] * (sem_input[j][3] * sem_input[j][8] - sem_input[j][5] * sem_input[j][6]) +
                            sem_input[j][2] * (sem_input[j][3] * sem_input[j][7] - sem_input[j][4] * sem_input[j][6]);
        }
        auto end_time_sem = high_resolution_clock::now();
        auto duration_sem = duration_cast<nanoseconds>(end_time_sem - start_time_sem).count();
        auto duration_sem_per_cycle = duration_sem / inner_cycles;
        acc_sem(duration_sem_per_cycle);
        // -------------------------------- End of "Semi" performance test -------------------------------------
*/
        // -------------------------------- Start of "Eigen" performance test -----------------------------------
        auto start_time_eig = high_resolution_clock::now();
        for (int j = 0; j < inner_cycles; ++j)
        {
            eig_result[j] = eig_input[j].determinant();
        }
        auto end_time_eig = high_resolution_clock::now();
        auto duration_eig = duration_cast<nanoseconds>(end_time_eig - start_time_eig).count();
        auto duration_eig_per_cycle = duration_eig / inner_cycles;
        acc_eig(duration_eig_per_cycle);
        // -------------------------------- End of "Eigen" performance test -------------------------------------

        // -------------------------------- Start of "Blas" performance test -----------------------------------
        auto start_time_bla = high_resolution_clock::now();
        for (int j = 0; j < inner_cycles; ++j)
        {
            auto matrix = bla_input[j].data();

            int info = 0;
            int ipiv[N];

#if defined(BLA_VENDOR_OPEN_BLAS)
            info = LAPACKE_dgetrf(LAPACK_ROW_MAJOR, M, N, matrix, N, ipiv);
#elif defined(BLA_VENDOR_APPLE)
            // Be aware: dgetrf_ is a function that assume a col major order, but for determinant it is the same
            int m = M, n = N;
            dgetrf_(&m, &n, matrix, &n, ipiv, &info);
#endif

            if (info != 0) throw std::runtime_error("LAPACK: error during calling of function _dgetrf");
            double determinant = 1.0;
            for (int i = 0; i < N; i++)
            {
                auto v = matrix[i * (N + 1)];
                determinant *= ipiv[i] != (i + 1) ? -v : v;
            }

            bla_result[j] = determinant;
        }
        auto end_time_bla = high_resolution_clock::now();
        auto duration_bla = duration_cast<nanoseconds>(end_time_bla - start_time_bla).count();
        auto duration_bla_per_cycle = duration_bla / inner_cycles;
        acc_bla(duration_bla_per_cycle);
        // -------------------------------- End of "Blas" performance test -------------------------------------

        // -------------------------------- Start of "Univec" performance test --------------------------------
        auto start_time_uni = high_resolution_clock::now();

        for (int j = 0; j < inner_cycles; ++j)
        {
            uni_result[j] = uni_input[j].det();
        }
        auto end_time_uni = high_resolution_clock::now();

        auto duration_uni = duration_cast<nanoseconds>(end_time_uni - start_time_uni).count();
        auto duration_uni_per_cycle = duration_uni / inner_cycles;
        acc_uni(duration_uni_per_cycle);
        // -------------------------------- End of "Univec" performance test ----------------------------------

        // -------------------------------- Check the results ----------------------------------
        for (int i = 0; i < inner_cycles; i++)
        {
            //if (abs(raw_result[i] - sem_result[i]  / QtySiLengthPowD::from_value(1.)) > 1e-5)
            //    throw std::runtime_error("failed validation for Semi" + to_string(raw_result[i]) + " " + to_string( sem_result[i] / cubic_meter)  );
            if (abs(QtySiLengthPowD(uni_result[i]) / QtySiLengthPowD::from_value(1.) - eig_result[i]) > 1e-9)
                throw std::runtime_error("failed validation for Eigen");
            if (abs(QtySiLengthPowD(uni_result[i]) / QtySiLengthPowD::from_value(1.) - bla_result[i]) > 1e-9)// Reducing needed precision to help lapack
                throw std::runtime_error("failed validation for Blas");
            //if (abs(raw_result[i] - QtySiLengthPowD(uni_result[i]) / QtySiLengthPowD::from_value(1.)) > 1e-5)
            //    throw std::runtime_error("failed validation for Univec");
        }
    }

    double mean_raw = mean(acc_raw);
    double mean_sem = mean(acc_sem);
    double mean_eig = mean(acc_eig);
    double mean_bla = mean(acc_bla);
    double mean_uni = mean(acc_uni);
    double std_dev_raw = sqrt(variance(acc_raw));
    double std_dev_sem = sqrt(variance(acc_sem));
    double std_dev_eig = sqrt(variance(acc_eig));
    double std_dev_bla = sqrt(variance(acc_bla));
    double std_dev_uni = sqrt(variance(acc_uni));

    // Print the performance results
    cout << "Result for running " << inner_cycles << " inner_cycles, with mean and stdev calculated for " << outer_cycles << " outer_cycles." << endl;
    cout << "   Raw: mean_time: " << mean_raw << " ns, stdev: " << std_dev_raw << " ns" << endl;
    cout << "  Semi: mean_time: " << mean_sem << " ns, stdev: " << std_dev_sem << " ns" << endl;
    cout << " Eigen: mean_time: " << mean_eig << " ns, stdev: " << std_dev_eig << " ns" << endl;
    cout << "  Blas: mean_time: " << mean_bla << " ns, stdev: " << std_dev_bla << " ns" << endl;
    cout << "Univec: mean_time: " << mean_uni << " ns, stdev: " << std_dev_uni << " ns" << endl;

    // Save the results to a file
    fstream results_file("result.csv", ios::app);
    if (!results_file.is_open())
    {
        cout << "Unable to open the results file:" << endl;
        return -1;
    }

    results_file.seekg(0, std::ios::end);

    // check if is the first line in the file
    if (results_file.tellg() == 0)
    {
        results_file << "Operation, Outer Iterations, Inner Iterations, Mean Raw, Std Raw, Mean Semi, Std Semi, Mean Eigen, Std Eigen, Mean Blas, Std Blas, Mean Univec, Std Univec" << endl;
    }

    results_file << filesystem::path(argv[0]).filename().string() << ","
                 << outer_cycles << ","
                 << inner_cycles << ","
                 << mean_raw << "," << std_dev_raw << ","
                 << mean_sem << "," << std_dev_sem << ","
                 << mean_eig << "," << std_dev_eig << ","
                 << mean_bla << "," << std_dev_bla << ","
                 << mean_uni << "," << std_dev_uni << endl;

    results_file.close();

    return 0;
}