#!/usr/bin/env python3

from pathlib import Path
import re
import pandas as pd
import plotly.graph_objs as go
from InquirerPy import inquirer
from natsort import natsort_keygen

data_folder = Path.cwd().parent
test_system = ''

files = []
for result_file in data_folder.glob('*.csv'):
    files.append(str(result_file.name))

files = sorted(files, reverse=True)

result_file = inquirer.select(
    message= "Select the result file to plot:",
    choices= files,
    default= None,
).execute()

if result_file:
    result_file_pattern = '^result_([a-z]*)\w*_\d*.csv$'
    # determine the operating system for the tests (macOS or linux)
    match = re.search(result_file_pattern, result_file)
    if match:
        test_system = 'macOS' if match.group(1) == 'darwin' else 'linux'

result_file = data_folder / result_file

# read the result files and create a dataframe
result_df = pd.read_csv(result_file)


# Rename the blas/lapack column
result_df.rename(columns={' Mean Blas': ' Mean Blas/Lapack'}, inplace=True)

colors_vec = ['#98eecc', '#00c1c5', '#0091c0', '#005eac', '#1a247c']
marker_symbols_vec = ['circle', 'square', 'diamond', 'triangle-up', 'triangle-down']
colors_mat = ['#98eecc','#0091c0', '#1a247c']
marker_symbols_mat = ['circle', 'square', 'diamond']

# Used to specify the figure size when saving the plot
fig_width = 1000
fig_height = 700
fig_scale = 4.0

# -------------------------------- Prepare vector scaling data -----------------------------------
# Filter out the vector operations
result_vector = result_df[result_df['Operation'].str.contains('vector')]
# Delete unwanted columns
result_vector = result_vector.drop(columns=[' Outer Iterations', ' Inner Iterations'], axis=1)

# Sort the dataframe by the operation name using natural sorting
result_vector.sort_values(by='Operation', key=natsort_keygen(), inplace=True)
result_vector.reset_index(drop=True, inplace=True)
# Delete the first row from results which is the same with matrix1x1
result_vector.drop(index=0, inplace=True)  # 10 is the index of vector_dot_1d

print(result_vector)

# Extract the dimension from the operation name
regex_dimension = re.compile(r'^vector_dot_(\d{1,2})d$')
vec_x_values= []
for index, row in result_vector.iterrows():
    dimension = regex_dimension.search(row['Operation']).group(1)
    vec_x_values.append(dimension)

# -------------------------------- Plot the vector results -----------------------------------
# Create and configure the plotly figure
fig_vector_scaling = go.Figure(
    layout= go.Layout(
        width= fig_width,
        height= fig_height,
        title= dict(
            # text= f"Vector dimension scaling - {test_system}",
            x= 0.5,
            y= 0.95,
            xanchor= 'center',
            yanchor= 'top',
            font= dict(size= 28),
        ),
        xaxis= dict(
            title= 'Vector Dimension',
            titlefont= dict(size= 28),
            showgrid= True,
            showline= True,
            linecolor= '#444',
            linewidth= 2,
            mirror= True,
            tickfont= dict(size= 20),
            ticks='inside',
        ),
        yaxis= dict(
            title= 'Time [ns]',
            titlefont= dict(size= 28),
            showgrid= True,
            showline= True,
            linecolor= '#444',
            linewidth= 2,
            mirror= True,
            tickfont= dict(size= 20),
            ticks='inside',
        ),
        legend= dict(
            title= 'Library:',
            font = dict(size= 18),
            orientation= 'h',
            yanchor= 'bottom',
            y= 1.02,
            xanchor= 'right',
            x= 1
        ),
    )
)
# Populate the figure with the data
for index in range(1, len(result_vector.columns), 2):
    col = result_vector.columns[index]
    fig_vector_scaling.add_scatter(
        name= col.replace('Mean ', ''),
        x= pd.Series(vec_x_values),
        y= result_vector.iloc[:, index],
        # error_y=dict(type='data', array=result_vector.iloc[:, index+1], visible=True),
        marker_color= colors_vec[index//2],
        marker_symbol=  marker_symbols_vec[index//2],
        marker_size= 10,
        marker_line_width= 1.5,
        marker_line_color= 'DarkSlateGrey',
    )

# fig_vector_scaling.show()
fig_vector_scaling.write_image(f'{test_system}/vector_scaling_{test_system}.png', width= fig_width, height= fig_height, scale= fig_scale)
print(f"Plot 'vector_scaling_{test_system}.png' was saved successfully in {test_system} directory")

# -------------------------------- Prepare matrix scaling data -----------------------------------
# Filter out the matrix operations
result_matrix = result_df[result_df['Operation'].str.contains('matrix')]
# Delete unwanted columns
result_matrix = result_matrix.drop(columns=[' Outer Iterations', ' Inner Iterations', ' Mean Raw', ' Std Raw', ' Mean Semi', ' Std Semi'], axis=1)
# Sort the dataframe by the operation name using natural sorting
result_matrix.sort_values(by='Operation', key=natsort_keygen(), inplace=True)
result_matrix.reset_index(drop=True, inplace=True)
# Delete the first row from results which is the same with matrix1x1
result_matrix.drop(index=0, inplace=True)  # 0 is the index of matrix1x1


result_vector.reset_index(drop=True, inplace=True)

print(result_matrix)

# Extract the dimension from the operation name
regex_dimension = re.compile(r'^matrix_determinant_(\d{1,2})x\d+$')
mat_x_values = []
for index, row in result_matrix.iterrows():
    dimension = regex_dimension.search(row['Operation']).group(1)
    mat_x_values.append(dimension)

# -------------------------------- Plot the matrix results -----------------------------------
# Create and configure the plotly figure
fig_matrix_scaling = go.Figure(
    layout= go.Layout(
        width= fig_width,
        height= fig_height,
        title= dict(
            # text= f"Matrix dimension scaling - {test_system}",
            x= 0.5,
            y= 0.95,
            xanchor= 'center',
            yanchor= 'top',
            font= dict(size= 28),
        ),
        xaxis= dict(
            title= 'Matrix Dimension',
            titlefont= dict(size= 28),
            showgrid= True,
            showline= True,
            linecolor= '#444',
            linewidth= 2,
            mirror= True,
            tickfont= dict(size= 20),
            ticks='inside',
        ),
        yaxis= dict(
            # type= 'log',
            title= 'Time [ns]',
            titlefont= dict(size= 28),
            showgrid= True,
            showline= True,
            linecolor= '#444',
            linewidth= 2,
            mirror= True,
            tickfont= dict(size= 20),
            ticks='inside',
        ),
        legend= dict(
            title= 'Library:',
            font = dict(size= 18),
            orientation= 'h',
            yanchor= 'bottom',
            y= 1.02,
            xanchor= 'right',
            x= 1
        ),
    )
)
# Populate the figure with the data
for index in range(1, len(result_matrix.columns), 2):
    col = result_matrix.columns[index]
    fig_matrix_scaling.add_scatter(
        name= col.replace('Mean ', ''),
        x= pd.Series(mat_x_values),
        y= result_matrix.iloc[:, index],
        # error_y=dict(type='data', array=result_vector.iloc[:, index+1], visible=True),
        marker_color= colors_mat[index//2],
        marker_symbol=  marker_symbols_mat[index//2],
        marker_size= 10,
        marker_line_width= 1.5,
        marker_line_color= 'DarkSlateGrey',
    )

# fig_matrix_scaling.show()
fig_matrix_scaling.write_image(f'{test_system}/matrix_scaling_{test_system}.png', width= fig_width, height= fig_height, scale= fig_scale)
print(f"Plot 'matrix_scaling_{test_system}.png' was saved successfully in {test_system} directory")