cmake_minimum_required(VERSION 3.9)
project(univec_scaling_test VERSION 1.0.0 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -Wno-unused-parameter -Wno-language-extension-token" )

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
   add_compile_options(-fconcepts -Wno-unknown-pragmas)
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "AppleClang")
   add_compile_options(-fdeclspec -Wno-pre-c++2b-compat)
endif ()

# This is needed by Eigen to remove a warning
add_compile_options(-Wno-deprecated-anon-enum-enum-conversion)

set(BUILD_MARCH "" CACHE STRING "Set the compiler -march flag: (empty), generic, native, ...")
if (NOT BUILD_MARCH STREQUAL "")
	add_compile_options("-march=${BUILD_MARCH}")
endif()

set(BUILD_MCPU "" CACHE STRING "Set the compiler -mcpu flag: (empty), apple-m2, ...")
if (NOT BUILD_MCPU STREQUAL "")
	add_compile_options("-mcpu=${BUILD_MCPU}")
endif()

include_directories(${PROJECT_SOURCE_DIR}/external/univec/include)
include_directories(${PROJECT_SOURCE_DIR}/external/qtydef/include)

set(DIMENSIONS 1 2 3 4 5 6 7 8 9 10)

set(EXE_FILES)

foreach(D ${DIMENSIONS})

	set(EXE_MATRIX_DETERMINANT matrix_determinant_${D}x${D})
	add_executable(${EXE_MATRIX_DETERMINANT}  src/matrix_determinant.cpp)
	target_compile_definitions(${EXE_MATRIX_DETERMINANT} PRIVATE MATRIX_M=${D})
	target_compile_definitions(${EXE_MATRIX_DETERMINANT} PRIVATE MATRIX_N=${D})
	list(APPEND EXE_FILES ${EXE_MATRIX_DETERMINANT})
	
	set(EXE_VECTOR_DOT vector_dot_${D}d)
	add_executable(${EXE_VECTOR_DOT}  src/vector_dot.cpp)
	target_compile_definitions(${EXE_VECTOR_DOT} PRIVATE VECTOR_D=${D})
	list(APPEND EXE_FILES ${EXE_VECTOR_DOT})

endforeach()


find_package(Eigen3 REQUIRED)
if (Eigen3_FOUND)
   include_directories( ${EIGEN3_INCLUDE_DIR} )

   foreach(EXE_FILE ${EXE_FILES})
      target_link_libraries(${EXE_FILE} PRIVATE Eigen3::Eigen)
   endforeach()
endif()

find_package(LAPACK REQUIRED)
if(LAPACK_FOUND)
   foreach(EXE_FILE ${EXE_FILES})
      target_link_libraries(${EXE_FILE} PRIVATE LAPACK::LAPACK)
   endforeach()
endif()

set(BLA_VENDOR Apple)
find_package(BLAS)
if(BLAS_FOUND)
   add_compile_definitions(BLA_VENDOR_APPLE)
   foreach(EXE_FILE ${EXE_FILES})
      target_link_libraries(${EXE_FILE} PRIVATE BLAS::BLAS)
   endforeach()

else()
   set(BLA_VENDOR OpenBLAS)
   find_package(BLAS)
   if(BLAS_FOUND)
      add_compile_definitions(BLA_VENDOR_OPEN_BLAS)
      foreach(EXE_FILE ${EXE_FILES})
         target_link_libraries(${EXE_FILE} PRIVATE BLAS::BLAS)
      endforeach()

      find_package(LAPACKE REQUIRED)
      if(LAPACKE_FOUND)
         include_directories(${LAPACKE_INCLUDE_DIRS})
         foreach(EXE_FILE ${EXE_FILES})
            # Linking the old style
            target_link_libraries(${EXE_FILE} PRIVATE ${LAPACKE_LIBRARIES})
         endforeach()
      endif()
   endif()
endif()

if (NOT BLAS_FOUND)
   message(FATAL_ERROR "Unable to find OpenBlas or Accelerate framework")
endif()

find_package(Boost 1.36 REQUIRED)
if (${Boost_FOUND})
   foreach(EXE_FILE ${EXE_FILES})
      target_link_libraries(${EXE_FILE} PRIVATE Boost::boost)
   endforeach()
endif ()

if(NOT EXISTS "${PROJECT_SOURCE_DIR}/external/univec/include")
   message(FATAL_ERROR "Unable to find the directory 'external/univec/include'.
Did you perform?
  git submodule init
  git submodule update")
endif()

if(NOT EXISTS "${PROJECT_SOURCE_DIR}/external/qtydef/include")
   message(FATAL_ERROR "Unable to find the directory 'external/qtydef/include'.
Did you perform?
  git submodule init
  git submodule update")
endif()

